numero = 10;
print(numero);
print(type(numero));
print('----------------');

numeroComaFlotante = 10.25;
print(numeroComaFlotante);
print(type(numeroComaFlotante));
print('----------------');

texto = 'Hola soy una serpiente 🐍';
print(texto);
print(type(texto));
print('----------------');

booleano = True;
print(booleano);
print(type(booleano));
print('----------------');

suma = numero + numeroComaFlotante;
print(suma);
print(type(suma));
print('----------------');

suma = texto;
print(suma);
print(type(suma));
print('----------------');
